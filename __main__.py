"""
Copyright © Morgan Leclerc (2019)

morgan.leclerc@caramail.com

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
"""

__author__ = 'Morgan Leclerc'
__version__ = '1.1 Stable'

import argparse
import os
import sys
from time import time, localtime, strftime

sys.path.append(os.getcwd())


try:
    from pymmodoro import sounds, start_pommodoros
except ImportError:
    raise ImportError('Cannot load pymmodoro, ensure the pymmodoro folder is in'
                      ' your PYTHONPATH')


parser = argparse.ArgumentParser(
    prog='pymmodoro',
    description='Be productive by using this advanced technique developed by'
                'fellow students called the pommodoros !')
parser.add_argument('-L', '--pommodoro-length', dest='pommodoro_length',
                    default=25, type=int,
                    help='Specify the length in minutes of one pommodoro. 25 '
                         'by default')
parser.add_argument('-l', '--pause-length', dest='pause_length',
                    default=5, type=int,
                    help='Specify the length in minutes of one pommodoro. 5 '
                         'by default')
parser.add_argument('-u', '--until', dest='until', default=None,
                    help='Specify when your session should stop. Your session '
                         'will not end later than the specified time but may '
                         'end sooner.\n'
                         'The time must follow the HH:MM 24h format or '
                         'HH:MMpp 12h format, and nothing else, where H is '
                         'the hour, M the minutes, S the seconds and p either '
                         'am or pm (non case-sensitive).\n'
                         'If specified time is sooner in the day than right '
                         'now, will jump to next day. If not specified, the '
                         'session never stops without a --count argument.'
                         'Hitting <Ctrl-C> ends the program nicely.')
parser.add_argument('-c', '--count', dest='count', type=int, default=-1,
                    help='Specify the maximum number of pommodoros of your '
                         'session. It will never exceed it but --until may stop'
                         ' your session sooner.\n'
                         'If not specified, the session never stops without an '
                         '--until argument. Hitting <Ctrl-C> ends the program '
                         'nicely')
parser.add_argument('-p', '--start-with-pause', dest='start_with_pause',
                    action='store_true',
                    help='Starts your session with a pause rather than a '
                         'pommodoro when specified.')
parser.add_argument('-t', '--tell-hour', nargs='?', dest='tell_hour',
                    const='24', default=None, choices=['12', '24'],
                    help='Tells you the hour at the end of each pommodoro, '
                         'using either 24h format (by specifying 24) or 12h '
                         'format (by specifying 12). If no value is given, uses'
                         ' 24h format.')
parser.add_argument('-T', '--no-timer', dest='timer', action='store_false',
                    help='Disable the display of remaining time of undergoing '
                         'pommodoro')
parser.add_argument('-r', '--record', dest='record', nargs='?',
                    default='no', const='csv', choices=['csv', 'markdown'],
                    help='Enables the recording of pommodoros for your stats.'
                         'Default value is "csv", other values my be '
                         '"markdown" (Storing as a markdown table) or "no" '
                         '(disabling record). When not ')
parser.add_argument('-s', '--display-stats', dest='display_stats',
                    action='store_true', help='Displays your stats and exits.'
                                              'Can only parse markdown '
                                              'storage files for now')
parser.add_argument('-C', '--starting-count', dest='starting_count', type=int,
                    default=1, help='Start at a certain pommodoro count,'
                                    'allowing you to restore from an '
                                    'interrupted session.')
parser.add_argument('--version', action='version', version=__version__)

args = parser.parse_args()

if args.display_stats:
    longest_session = 0
    average_session = []
    average_pommodoro_length = []
    average_pause_length = []
    n = 0

    session_count = 0
    session_length = 0
    with open('pymmodoro/pymmodoro.md', 'r') as stats_file:
        stats_file.readline(), stats_file.readline() # skip header
        line = stats_file.readline()
        while line:
            line = [field.strip() for field in line.split('|')]
            n += 1
            count = int(line[1])
            pommodoro_length = int(line[2])
            pause_length = int(line[3])

            if count < session_count:
                average_session += [session_count]
                if session_length > longest_session:
                    longest_session = session_length
                session_length = pommodoro_length + pause_length
            else:
                session_length += pommodoro_length + pause_length
            session_count = count
            average_pommodoro_length += [pommodoro_length]
            average_pause_length += [pause_length]
            line = stats_file.readline()

        average_session += [session_count]
        if session_length > longest_session:
            longest_session = session_length
        average_session = sum(average_session)/len(average_session)
        average_pause_length =\
            sum(average_pause_length)/len(average_pause_length)
        average_pommodoro_length\
            = sum(average_pommodoro_length)/len(average_pommodoro_length)
        print(f'Average session: {average_session} pommodoros')
        print(f'Longest session: {longest_session} minutes')
        print(f'Average pommodoro length: {average_pommodoro_length} minutes')
        print(f'Average pause length: {average_pause_length} minutes')
        print(f'You did a total {n} pommodoro')
    exit(0)

pommodoro_length = args.pommodoro_length
pause_length = args.pause_length
max_count = args.count
tell_hour = args.tell_hour
start_with_pause = args.start_with_pause
timer = args.timer
record = args.record
starting_count = args.starting_count

str_time = args.until
if str_time is not None:
    from time import strptime, localtime

    str_time = str_time.lower()

    if str_time.endswith('am') or str_time.endswith('pm'):
        fmt = '%I:%M%p'
    else:
        fmt = '%H:%M'

    target_hour, target_minute = strptime(str_time, fmt)[3:5]
    current_hour, current_minute = localtime()[3:5]

    if current_hour > target_hour:
        target_hour += 24

    count = int((target_hour*60+target_minute-(current_hour*60+current_minute))
                / (pommodoro_length + pause_length)) + starting_count
    if max_count >= 0:
        count = min(max_count, count)
else:
    count = max_count

if count >= 0:

    until = strftime('%H:%M',
                     localtime(time() + 60 * (
                                 count * (pommodoro_length + pause_length)
                                 - pause_length + 1)))

    msg = f'Starting a pommodoro session of {count} pommodoros until {until} '

else:
    msg = 'Starting a pommodoro session of many pommodoros '

msg += f'(alternating {pommodoro_length}m of work with {pause_length}m of ' \
    'pause)\n'

if start_with_pause:
    msg += ' We start with a pause'

    if tell_hour:
        msg += ' and will tell you the time at each pommodoro\'s end.'
    else:
        msg += '.'

elif tell_hour:
    msg += ' We will tell you the time at each pommodoro\'s end.'

if starting_count > 1:
    msg += f' Starting at pommodoro {starting_count}.'

msg += ' Have a great work session !\n'

print(msg)

try:
    start_pommodoros(count, pommodoro_length, pause_length, starting_count,
                     timer, start_with_pause, tell_hour, record)
except KeyboardInterrupt:
    print('\nYou have ended your work session.')
else:
    print('Your work session is over, congratulations !')
